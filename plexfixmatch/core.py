#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -----------------------------------------------------------------------------
#
# MIT License
#
# Copyright (c) 2018 Florent TOURNOIS
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# -----------------------------------------------------------------------------

import logging
import sys
import os
import os.path
import re
import upref
import plexapi.server


__YEAR__ = r"(19[3-9][0-9])|(20[012][0-9])"
__IMDBID__ = r"[tT]{2}[0-9]{7}"


# -----------------------------------------------------------------------------
# define a static decorator for function
#
# @code{.py}
# 	(at)static(__folder_md_test__=None)
# 	def get_test_folder():
#     		if get_test_folder.__folder_md_test__ is None:
#         	get_test_folder.__folder_md_test__ = check_folder(os.path.join(
#             		os.path.split(__get_this_filename())[0], "test-md"))
#     		return get_test_folder.__folder_md_test__
# @endcode
#
# @param kwargs list of arguments
# @return the wrap function
# -----------------------------------------------------------------------------
def static(**kwargs):
    def wrap(the_decorated_function):
        for key, value in kwargs.items():
            setattr(the_decorated_function, key, value)
        return the_decorated_function
    return wrap


# -----------------------------------------------------------------------------
# Find the plex server object
#
# @param name the shortcut name of the server if there is many.
# @return the plex object
# -----------------------------------------------------------------------------
@static(__links__=None)
def server(name="default", renew=False):
    if server.__links__ is None:
        server.__links__ = {}

    if name in server.__links__ and not renew:
        return server.__links__[name]

    server.__links__[name] = __create_server(name)
    return server.__links__[name]


# -----------------------------------------------------------------------------
# @param name the shortcut name of the server if there is many.
# @return the plex object
# -----------------------------------------------------------------------------
def __create_server(name):
    logging.info('Connect to the server %s', name)

    server_conf_model = upref.load_conf(
        os.path.join(__get_this_folder(), "server.conf"))
    conf = upref.get_pref(server_conf_model, "plex_server_%s" % name)

    return plexapi.server.PlexServer(conf['url'], conf['token'])


# -----------------------------------------------------------------------------
# @param name the shortcut name of the server if there is many.
# @return the plex object
# -----------------------------------------------------------------------------
def movies_sections(pserver):
    return [sect for sect in pserver.library.sections()
            if sect.type == "movie"]


# -----------------------------------------------------------------------------
# @param name the shortcut name of the server if there is many.
# @return the plex object
# -----------------------------------------------------------------------------
def unmatch_movies(name="default"):
    result = []
    plex = server(name)
    for section in plex.sections():
        logging.info("search in %s", section.title)


# -----------------------------------------------------------------------------
# @param name the shortcut name of the server if there is many.
# @return the plex object
# -----------------------------------------------------------------------------
def discover(expression):
    def from_txt(txt):
        found = [x.group() for x in re.finditer(expression, txt)]
        found = unique_value([x.lower() for x in found])
        if len(found) == 1:
            return found[0]
        return None

    return from_txt


# -----------------------------------------------------------------------------
def unique_value(the_list):
    used = set()
    return [x for x in the_list if x not in used and (used.add(x) or True)]


# -----------------------------------------------------------------------------
def not_none(the_list):
    return [x for x in the_list if x is not None]


# -----------------------------------------------------------------------------
def imdbid_search(movie):
    discover_imdbid = discover(__IMDBID__)
    found = [
        discover_imdbid(movie.title),
        discover_imdbid(movie.guid)
    ]
    found.extend([discover_imdbid(loc) for loc in movie.locations])
    found = not_none(unique_value(found))
    if len(found) == 1:
        return found[0]
    return None


# -----------------------------------------------------------------------------
def imdbid_year(movie):
    discover_year = discover(__YEAR__)
    found = [
        discover_year(movie.title),
    ]
    found.extend([discover_year(loc) for loc in movie.locations])
    found = not_none(unique_value(found))
    if len(found) == 1:
        return found[0]
    return None


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the filename of THIS script.
# -----------------------------------------------------------------------------
def __get_this_filename():
    return sys.executable if getattr(sys, 'frozen', False) else __file__


# -----------------------------------------------------------------------------
# Find the filename of this file (depend on the frozen or not)
# This function return the filename of this script.
# The function is complex for the frozen system
#
# @return the folder of THIS script.
# -----------------------------------------------------------------------------
def __get_this_folder():
    return os.path.split(os.path.abspath(os.path.realpath(
        __get_this_filename())))[0]
